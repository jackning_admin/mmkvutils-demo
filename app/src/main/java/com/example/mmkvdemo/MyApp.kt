package com.example.mmkvdemo

import com.example.lib_common.base.BaseApplication

/**
 * @author: njb
 * @date: 2023/8/9 23:19
 * @desc:
 */
class MyApp :BaseApplication(){
    override fun onCreate() {
        super.onCreate()
        mInstance = this
       // initMMKV()
        initARouter()
    }



    companion object{
        lateinit var mInstance: MyApp
            private set
    }
}