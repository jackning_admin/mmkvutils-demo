package com.example.testmmkv.app;

import com.example.lib_common.base.BaseApplication;

/**
 * @author: njb
 * @date: 2023/8/11 0:10
 * @desc:
 */
public class TestApp extends BaseApplication {
    private static TestApp mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        //initMMKV();
    }

    public static TestApp getInstance() {
        return mInstance;
    }
}
