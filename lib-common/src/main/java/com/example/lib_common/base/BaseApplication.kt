package com.example.lib_common.base

import android.app.Application
import com.alibaba.android.arouter.launcher.ARouter
import com.example.lib_common.BuildConfig
import com.example.lib_common.utils.MMKVUtil
import com.example.lib_common.utils.MMKVUtils

/**
 * @author: njb
 * @date: 2023/8/11 0:23
 * @desc:
 */
open class BaseApplication :Application(){
    override fun onCreate() {
        super.onCreate()
        Instance = this
        initMMKV()
        //initARouter()
    }

     fun initARouter() {
         if (BuildConfig.DEBUG) {
             ARouter.openLog();
             ARouter.openDebug();
         }
        ARouter.init(this)
    }

    fun initMMKV(){
        MMKVUtils.getInstance().init(this)
    }
    fun initMMKVTest(){
        //MMKVUtil.instance.initTest(this)
    }

    companion object{
        lateinit var Instance: BaseApplication
            private set
    }

    override fun onTerminate() {
        super.onTerminate()
        ARouter.getInstance().destroy()
    }
}