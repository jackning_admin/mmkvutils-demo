package com.example.lib_common.router

import android.os.Bundle
import com.alibaba.android.arouter.launcher.ARouter

/**
 * @author: njb
 * @date: 2023/8/24 14:29
 * @desc:
 */
object RouterCenter {
    fun toTest() {
        ARouter.getInstance().build(RouterURLS.TEST_ACTIVITY).navigation()
    }
}