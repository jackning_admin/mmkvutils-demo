package com.example.testarouter

import android.app.Application
import com.example.lib_common.base.BaseApplication

/**
 * @author: njb
 * @date: 2023/8/24 14:25
 * @desc:
 */
class TestARouterApp :BaseApplication(){
    override fun onCreate() {
        super.onCreate()
        initARouter()
    }
}